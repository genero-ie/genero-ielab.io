# Comisión de Equidad de Género del IE 
 
Aprobada por el Consejo Interno del IE en su sesión ordinaria del 28
de febrero de 2020
         
De acuerdo con los lineamientos generales para la Igualdad de Género
en la UNAM, publicados en la Gaceta UNAM el día 7 de marzo de 2013, la
Política Institucional de Género tiene como objetivo proporcionar a
las autoridades y al cuerpo directivo de la Universidad Nacional
Autónoma de México (UNAM) herramientas de trabajo que permitan la
construcción de la igualdad de género en nuestra Máxima Casa de
Estudios.
 
La Universidad Nacional Autónoma de México (UNAM) ha impulsado, a lo
largo de décadas, distintas iniciativas encaminadas a eliminar la
discriminación por razones de género y promover la participación
equitativa entre mujeres y hombres en todas las funciones sustantivas
de nuestra Institución. Además, ha sido pionera en el establecimiento
de instancias destinadas a la promoción de la equidad e igualdad de
género entre quienes la conforman. Particularmente, la Comisión
Especial de Equidad de Género del H. Consejo Universitario (CEEG) es
responsable de diseñar las directrices de la política institucional, y
el Centro de Investigaciones y Estudios de Género está encargado de
desarrollar las capacidades necesarias para acompañar este proceso
desde la producción académica y el diseño de modelos de análisis.
                                                                                   
Para seguir avanzando en el proceso de institucionalización de la
perspectiva de género en las políticas, programas y en la vida
cotidiana de la comunidad universitaria, dadas las características
plurales y la magnitud de nuestra Casa de Estudios, se recomendó crear
Comisiones Internas de Equidad de Género en las distintas entidades
académicas y dependencias universitarias que coadyuven en la
implementación de las políticas promovidas por la CEEG.
 
Tomando en consideración lo anterior, con la iniciativa impulsada por
la Dirección del Instituto de Ecología y partiendo de la inquietud e
interés de algunos miembros de su comunidad, asistentes al taller
Pensemos Todas y Todos el Feminismo (primer taller llevado a cabo en
la Investigación científica, dedicado a introducir a sus participantes
al estudio de las problemáticas de género), para llevar a cabo
acciones preventivas relacionadas con dicha problemática, el Consejo
Interno dictaminó la constitución de la Comisión de Equidad de Género
del Instituto de Ecología.
 
Los integrantes de la Comisión de Equidad de Género formarán parte de
la misma por un período de cuatro años, una vez concluido ese periodo,
el Consejo Interno decidirá el procedimiento para su renovación.
 
La primera Comisión de Equidad de Género del Instituto de Ecología,
quedó integrada por las siguientes personas:
 

     
01    Laura Espinosa Asuar                  Coordinadora

02    Roxana Torres Avilés                  Representante de Coordinación de Docencia y FRH
        
03    Alejandro González Voyer              Representante de Tutores
        
04    Ruth Percino Daniel                   Representante de Estudiantes
        
05    Pablo Lavaniegos Puebla               Representante de Estudiantes
        
06    Rodrigo García Herrera                Representante de Técnicos Académicos
        
07    Graciela García Guzmán                Representante de Secretaría Técnica
        
08    Éricka Esquivel Valdepeña y López     Representante de Funcionarios, Personal de Confianza y Administrativos
        
09    Erika Aguirre Planter                 Representante de Comité de Ética
        
10    Diana G. Martínez                     Representante de Personal de Base
   
 
 
Con la integración de todos los miembros arriba mencionados, quedan
representados la mayoría de los sectores de nuestra comunidad con la
finalidad de establecer acciones acordes a aquellas emitidas por la
Comisión Especial de Equidad del H. Consejo Universitario.
 
Se instaurará la figura de “voluntario”, consistiendo en aquéllas
personas de la comunidad convocadas por la propia comisión, quienes
apoyarán en el desarrollo de proyectos específicos de la misma.
 
La Comisión de Equidad de Género sesionará dos veces por semestre y
de forma extraordinaria si fuera necesario.
 
 
# Objetivo general de la Comisión de Equidad de Género del IE
 
Impulsar al interior del Instituto de Ecología la Política
Institucional de Género (PIG), con base en las directrices elaboradas
por las comisiones especiales de Equidad de Género y de Seguridad del
H. Consejo Universitario (CEEG y CES), en armonía con los lineamientos
generales para la Igualdad de Género en la UNAM, y con apego al
Protocolo para la Atención de casos de Violencia de Género en la UNAM,
y todos los mecanismos, protocolos y lineamientos emitidos en la
Universidad, adecuando las propuestas a las características propias de
cada entidad y dependencia, a fin de lograr la igualdad y la equidad
de género.
 
 
## Funciones
 
 - Diseñar y poner en marcha un plan de trabajo anual con base en la
   Política Institucional de Género elaborada por la CEEG. La
   propuesta del plan de trabajo se hará considerando los contextos
   particulares del IE.
 
 - Impulsar la incorporación de la perspectiva de género en los planes
   de desarrollo y seguridad en el contexto del IE
   
 - Mantener un registro actualizado de todas las actividades que se
   realizan en el IE sobre temas de género, organizadas por la Comisión. #Quité lo que no haremos nosotros y es más fácil si sólo registramos lo que hagamos, pero el IE va a necesitar un registro de las otras actividades, hay que preguntar quién lo hará...
 
 - Coordinar y asegurar la acreditación como orientadores a 2 de sus miembros como mínimo,
   para que a través de ellos exista la posibilidad de canalizar
   las denuncias de violencia de género a las autoridades pertinentes,
   y acompañar debidamente los casos de violencia de género que
   pudieran presentarse en nuestra comunidad.
 
 - Desarrollar e implementar acciones en la comunidad del IE que se
   complementen con las acciones del plan de trabajo anual, orientadas
   a la prevención de problemáticas relacionadas a la
   inequidad de género.
 
 - Entregar un informe anual al Director(a) del IE, quien a su vez lo turnará a la CEEG, y en su caso a
   la CES en el formato que la CEEG establezca.
 
 
## Vinculación
 
La Comisión de Equidad de Género, con el apoyo de la persona titular
de la Entidad Académica, atenderá a las Políticas Institucionales
diseñadas y recomendadas por la CEEG, mantendrá vinculación con la
Comisión Especial y podrán asesorarse con el Centro de Investigaciones
y Estudios de Género (CIEG).

## Plan de Trabajo General

- 1. Coordinar con la dirección el desarrollo de medidas pertinentes para evaluar la situación de la equidad de género en la entidad, instrumentar medidas pertinentes para erradicar las brechas de género encontradas; así mismo coordinar con la dirección un sistema de evaluación  (diagnóstico cualitativo y cuantitativo) con estrategias para prevenir y erradicar la violencia de género: encuesta, diagnóstico..
- 2. Dar seguimiento permamente a las estrategias de prevención y los sistemas de evaluación mencionados en el primer punto, relacionados con equidad de género y violencia de género 
- 3. Coordinar con el personal administrativo para que los cursos de capacitación y profesionalización incluyan contenidos de equidad de género
- 4. Coordinar junto con la dirección que se realice la capacitación del personal de vigilancia, académicos y estudiantes, así como personal administrativo y de base en nociones de violencia y equidad de género
- 5. Sugerir la incorporación de la perspectiva de género en los planes y programas de estudio
- 6. Coordinar con el CI y docencia la consideración de tiempos de estudio y laborales que implican las diferentes etapas de las mujeres
- 7. Desarrollar campañas permanentes, proyectos y acciones que sensibilicen y promuevan equidad y no violencia de género, que propicien la eliminación de estereotipos de género, y difundan información sobre atención y sanción en caso de violencia de género (recolección y publicación periódica de testimonios, seminario, taller, curso de sensibilización, conferencia, coloquio, conversatirio, jornadas, actividades culturales (cineclub, obra de tatro, danza, concierto, exposición...)
- 8. Coordinar medidas necesarias para conformar un entorno seguro dentro del centro de trabajo
- 9. Difusión de información en redes (p. ej. organización de publicaciones periódicas en pantallas (difusión del documento Política Institucional de Género, recolección de frases y su publicación, imágenes carteles y gifs que estaban en proceso de seleccionarse, impresión de algunos carteles para situarlos en puntos estratégicos)
- 10.Coordinar con la dirección la incorporación de un lactario y un baño incluyente en el IE (Rox: de hecho no creo que sea seguro, ni que este considerado por los reglamentos laborales la presencia en la universidad de bebes o niños muy pequeños. Lau: Rox, los lactarios son para que las mamás que están lactando puedan sacarse leche en un espacio seguro, no son para alimentar ahí a los bebés, pero este tema puede discutirse...) 

## Plan de Trabajo Anual

- 1 y 2. Coordinación con la dirección para desarrollar estrategias de evaluación de la situación tanto de equidad de género como de violencia de género, instrumentar medidas y dar seguimiento.
- 3 y 4. Permanecer en comunicación con el representante sindical, así como con la dirección y la coordinación de posgrado para incorporar contenido de género en cursos de capacitación a la comunidad del IE.
- 5. Elaborar un documento que sugiera la incorporación de la perspectiva de género en los planes y programas de estudio
- 7. Recolección y publicación periódica de testimonios.
- 9. Difusión del documento Política Institucional de Género, recolección de frases y su publicación en pantallas, imágenes carteles y gifs que estaban en proceso de seleccionarse, impresión de algunos carteles para situarlos en puntos estratégicos.


