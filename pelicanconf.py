#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Comisión Interna para la Igualdad de Género'
SITENAME = 'CInIG-IE'
#SITESUBTITLE = 'Comisión'
SITEURL = ''

PATH = 'content'


STATIC_PATHS = ['media', ]

TIMEZONE = 'Mexico/General'

DEFAULT_LANG = 'es'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (
    ('Noticias He/She',
     'https://noticiasieunam.blogspot.com/search/label/G%C3%A9nero',
     'logo_he_for_she.png'
    ),
    ('Espacio de orientación y atención psicológica',   # 'ESPORA Psicológica',
     'http://www.ecologia.unam.mx/web/index.php/espora',
     'logo_espora.png'
    ),
    
)

# Social widget
# SOCIAL = (('You can add links in your config file', '#'),
#           ('Another social link', '#'),)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True

THEME = 'theme'


#PAGE_URL = 'pages/{slug}/'
#PAGE_SAVE_AS = 'pages/{slug}/index.html'
