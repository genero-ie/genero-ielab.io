---
title: Sensibilicémonos, menopausia sin estigmas 
date: 2022-11-14
category: Comunicados
---

Seminarios Interinstitucionales de Igualdad de Género les invitan este proximo jueves 17 de noviembre a las 15:00 hrs a la conferencia virtual "Sensibilicémonos, menopausia sin estigmas".


<img src='/media/banner_conf_CInIG_17-Nov-22_Mod.jpg'width="90%"> 


**Video del seminario:**

<iframe width="560" height="315" src="https://www.youtube.com/embed/M4KhLxgKjF4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

