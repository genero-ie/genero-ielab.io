---
title: Casting docuserie LGBT+
date: 2023-02-14
category: Comunicados 
---

La Coordinación para la Igualdad de Género, en colaboración con Procine, invitan a todas las **diversidades y disidencias sexogenéricas** de la Universidad a participar en la miniserie documental "La UNAM sin límites" que se grabará a partir del 20 de febrero de 2023. Puedes participar contando tu testimonio o compartiendo algún talento, o también compartiendo esta imagen para que más gente se interese y participe. 

El link de registro es: <https://bit.ly/Preregistro-Casting> 

Tienes hasta el **16 de febrero de 2023**

<img src='/media/Docuserie_lgbt.jpg'width="90%">
