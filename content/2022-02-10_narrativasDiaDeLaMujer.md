---
title: Desafiemos discursos y narrativas en este día de la mujer y la niña en la ciencia.
date: 2022-02-10
category: Comunicados
---


Les invitamos al seminario que se llevará a cabo hoy 10 de febrero a las 17:00 titulado **Desafiemos discursos y narrativas en este día de la mujer y la niña en la ciencia.**




<img src='/media/la_mujer_y_la_niña_en_la_ciencia.jfif'width="50%"> 


**Video del seminario:**


<iframe width="560" height="315" src="https://www.youtube.com/embed/i3vYM1D0nV8?start=1" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

