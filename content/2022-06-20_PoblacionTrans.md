---
title: Formando un espacio seguro para la población trans
date: 2022-06-20
category: Comunicados
---


Les invitamos al seminario que se llevará a cabo mañana 21 de junio a las 9:30, titulado **Formando un espacio seguro para la población trans.**




<img src='/media/poblacionTrans.jfif'width="50%"> 


**Video del seminario:**


<iframe width="560" height="315" src="https://www.youtube.com/embed/MdNXgyH0Xao?start=1" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
