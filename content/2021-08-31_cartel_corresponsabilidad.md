---
title: Corresponsabilidad de los cuidados y labores domésticas
date: 2021-08-31
category: Comunicados
---

La Coordinación para la Igualdad de Género de la UNAM invita al
seminario interinstitucional de género **"Corresponsabilidad de los
cuidados y labores domésticas"**, el cual será impartido por el
Mtro. Rubén Hernández Duarte, el miércoles 01 de septiembre a las
12:00hrs.

<img src="/media/cartel_corresponsabilidad.jpg" width="100%" />



Este seminario es organizado de manera conjunta por las Comisiones
Internas de Igualdad de Género (CInIG) de los Institutos de Biología,
Ecología, Fisiología Celular, Investigaciones Biomédicas y
Matemáticas. 


**Video del seminario:**


<iframe width="560" height="315" src="https://www.youtube.com/embed/4F2uavD6liY?start=1" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


¿Tienes alguna duda? <cinig@iibiomedicas.unam.mx>

