---
title: Nueva dirección para esta página
date: 2020-08-21
category: Comunicados
authors: Ruth, Pablo, Rodrigo García
---

Nos es grato informarles que nuestra página ya está publicada bajo el dominio del [Instituto de Ecología](http://www.ecologia.unam.mx/).

La nueva dirección es:

[genero.ecologia.unam.mx](https://genero.ecologia.unam.mx)

:-)

