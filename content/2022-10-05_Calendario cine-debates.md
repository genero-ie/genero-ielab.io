---
title: Calendario de cine-debates
date: 2022-10-05
category: Comunicados
---

Les compartimos el calendario de las sesiones de cine debate de este semestre. 
Para participar escribanos al correo cinecinig@gmail.com para facilitar la película y el link de zoom.

Saludos, ¡esperamos verles pronto!

<img src='/media/Cartel_mensual_cinedebate__1_.png'width="90%">

