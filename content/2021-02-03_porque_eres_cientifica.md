---
title: ¿Por qué eres científica?
date: 2021-02-03 12:00
category: Comunicados
---

Los invitamos a conmemorar el Día Internacional de la Niña y la Mujer
en la Ciencia, este 11 de febrero a las 17 hrs, con la actividad
"¿Por qué eres científica? Pregúntale a Valeria", en la que niñas y
adolescentes conversarán con la investigadora Valeria Souza sobre su
trayectoria en la ciencia. Puedes participar en zoom (regístrate), o conectarte por youtube (abajo del formulario de registro encontrarás la liga). 



<img src="/media/2021-02-11-cientifica.png" width="100%" />

<iframe width="560" height="315" src="https://www.youtube.com/embed/bRj59YpzsRY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>



