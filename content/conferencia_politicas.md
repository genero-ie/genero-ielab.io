---
title: Conferencia del Curso Políticas Universitarias impartida por Rita Segato
date: 2020-08-05 12:00
category: Comunicados
author: Rodrigo García
---

Compartimos el video de la conferencia.

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/-8fiE_3q7mw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>