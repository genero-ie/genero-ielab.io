---
title: Editatón Wikipedia en la UNAM:Celebrando a nuestras científicas agregándolas a la enciclopedia libre
date: 2023-01-07
category: Comunicados
---

Les invitamos el próximo 10 de febrero al Editatón en el Instituto de Biomédicas.

[Link de registro](https://tinyurl.com/EditatonUNAM)

Para mayor información [aquí](https://www.gaceta.unam.mx/convocan-a-editaton-para.../)

<img src='/media/editaton_wikipedia_11F.jpeg'>
