---
title: Del ataque a nuestro seminario
date: 2020-04-21
category: Comunicados
---

Estimada comunidad, visitantes y colaboradores del Instituto de Ecología, UNAM:


La Comisión de Equidad y Género del Instituto de Ecología de la UNAM,
repudia el ataque ocurrido durante el seminario transmitido a través
de la plataforma Zoom el 17 de abril del presente año. La sala virtual
donde se llevaba a cabo el seminario fue vandalizada, mostrando
pornografía infantil, una forma de violencia de género, que como
todas, es inadmisible en nuestra sociedad. Este ataque a nuestra
comunidad se ha reportado a las Autoridades correspondientes.


Queremos expresar nuestra solidaridad con las personas que se
sintieron vulneradas y violentadas a raíz de este ataque, el cual nos
lastima, nos hiere y nos indigna como comunidad universitaria. El
repudio de actos deplorables como éste nos une y nos llama a hacer
conciencia social de un problema serio.

 - [Blog de Lydia Cacho](https://www.lydiacacho.com/LydiaCachosBlog/)
 
 - [La seguridad de los niños en línea: retos y estrategias mundiales](https://www.unicef-irc.org/publications/658-la-seguridad-de-los-ni%C3%B1os-en-l%C3%ADnea-retos-y-estrategias-mundiales.html)

Exhortamos a la comunidad, a los visitantes y colaboradores del IE
UNAM a reflexionar, a permanecer alertas y a pronunciarse en contra de
cualquier tipo de violencia de género.
