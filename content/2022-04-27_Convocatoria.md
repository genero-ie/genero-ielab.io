---
title: Convocatoria para formar parte de la CInIG IE (cerrada)
date: 2022-04-27
category: Comunicados
---

¿Eres personal administrativo de confianza o funcionariado del IE? 

¿Quieres ser parte de la Comisión Interna para la Igualdad de Género del Instituto de Ecología?

Descarga la [convocatoria](/media/convocatoria_cinig_2022_personalAdmn.pdf), envía tu solicitud.

La fecha límite es el próximo 9 de mayo

