---
title: Convocatoria para formar parte de la CInIG del IE
date: 2021-03-02
category: Comunicados
---

(CERRADA) ¿Quieres ser parte de la Comisión Interna para la Igualdad de Género del Instituto de Ecología?


Descarga la [convocatoria](/media/convocatoria_para_cinig.pdf), envía tu solicitud.


La fecha límite es el próximo 8 de marzo, Día Internacional de la
Mujer.

