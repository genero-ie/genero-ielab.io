---
title: Calendario mensual del Círculo de Lectura: De Feminismos, Diversidades y Otras Cosas.
fecha: 2022-09-13
category: Comunicados
---

Les compartimos el calendario mensual del **Círculo de Lectura: De Feminismos, Diversidades y Otras Cosas**, para el periodo septiembre - octubre 2022.


<img src='/media/Calendario_de_círculo_de_lectura_sept-oct.jpg'width="90%">

