---
title: Invitación videoglosario
date: 2023-05-17
category: Comunicados
---


Como parte de las actividades para conmemorar el mes del Orgullo LGBTIQ+, y a partir del 17 de mayo, "Día Internacional contra la Homofobia, Transfobia y Bifobia", te invitamos a ser parte de nuestro videoglosario de diversidades. ¡Aún tenemos mucho por conocer y aprender!

Participa enviando un video corto (máximo 5mn), narrando/explicando la definición de una palabra que consideres de relevancia para ser añadida, sin repetir las ya existentes. Pica aquí para revisar las que ya tenemos [Videoglosario](https://genero.ecologia.unam.mx/category/videoglosario.html)

La recepción de videos será a partir del 17 de mayo, y durante todo junio.

 Envía tu contribución a: actividades_cinig@ecologia.unam.mx

