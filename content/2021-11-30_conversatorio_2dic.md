---
title: Sesgos de género en la producción del conocimiento científico
date: 2021-11-30
category: Comunicados
---


Como parte de las actividades del #25N, **este jueves 2 de diciembre a las 17 hrs** les invitamos a la clausura semestral de los seminarios interinstitucionales de igualdad de género, que llevaremos a cabo con el conversatorio virtual en vivo titulado 
**Sesgos de género en la producción
del conocimiento científico**.




<img src='/media/Banner_CInIG_Conv_2-dic-21_500px.jpg'> 


**Video del seminario:**


<iframe width="560" height="315" src="https://www.youtube.com/embed/Rv9pA5ypzOM?start=1" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
