---
title: Calendario mensual del círculo de lectura noviembre-diciembre
date: 2022-11-01
category: Comunicados
---

Les compartimos el calendario mensual del Círculo de Lectura: De Feminismos, Diversidades y Otras Cosas, para el periodo noviembre - diciembre 2022.

<img src='/media/circulo_de_lectura_mensual.png'width="50%"> 
