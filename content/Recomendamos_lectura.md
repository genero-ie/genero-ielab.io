---
title: Recomendamos lectura
date: 2020-11-10
category: Comunicados
---

Recomendamos esta lectura para reflexionar sobre lo que podríamos hacer en nuestras propias instituciones y contextos. Agradecemos a Mariana Benítez, investigadora de nuestra comunidad, por haberlo compartido. 


[Kreeger, P. K. et. al. (2020). Ten simple rules for women principal investigators during a pandemic. PLOS Computational Biology, 16-10](https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1008370)
