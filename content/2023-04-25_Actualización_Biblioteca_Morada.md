---
title: Nuevos libros en la Biblioteca Morada
date: 2023-04-25
category: Comunicados
---

Les invitamos a acercarse a la Biblioteca Morada, ubicada dentro de la biblioteca del IE.

Desde la inauguración contamos con nuevos títulos, algunos en formato video-documental:

 - A través de los ojos de ella -Brianda Domecq

 - dientedeleón -María Romeu

 - El papel de la mujer en la historia -Academia Mexicana de la Historia

 - Guía para la incorporación de la perspectiva de género -SRE México

 - hojalata y lámina -María Romeu
 
 - La controversia del aborto desde la perspectiva de la razón pública -Itzel Mayans Hermida
 
 - Las celdas rosas -Sylvia Arvizu
 
 - Las malas — Camila Sosa Villada
 
 - Las mexicanas y el trabajo III. Hostigamiento sexual -INMUJERES
 
 - Mujeres Indígenas y violencia doméstica: Del silencio privado a las agendas públicas -CNDH 
 
 - Mujeres. Relatos de amor propio y dignidad -Carol Rossetti
 
 - Pequeños delirios grandes ausencias -Rubén Fischer
 
 - Señora de la noche -Osiris Gaona 
 
 - Unbearable weight. Feminism, western culture and the body -Susan Bordo

      

