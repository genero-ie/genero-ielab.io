---
title: Seminario interinstitucional de igualdad de género
date: 2022-03-10
category: Comunicados
---
Les invitamos al seminario que hoy se llevará a cabo HOY a las 17 hrs, como parte de las actividades que hemos organizado para conmemorar el Día de la Mujer.


<img src='/media/Seminario_Alba.jpeg' width="90%">


**Video del seminario:**


<iframe width="560" height="315" src="https://www.youtube.com/embed/_NvQJiocKPY?start=1" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
