---
title: Sobre acoso y hostigamiento
date: 2021-11-02
category: Comunicados
---
Durante el mes de noviembre estaremos compartiendo infografías, información y actividades relacionadas con violencia de género, en el marco del **día Internacional de la Eliminación de la Violencia contra la Mujer** que se conmemora el 25 de noviembre. 

<img src='/media/Acoso_y_hostigamiento.jpg'>

