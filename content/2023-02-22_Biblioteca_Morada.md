---
title: Biblioteca Morada
date: 2023-02-22
category: Comunicados 
---

Les invitamos a la inauguración de la Biblioteca Morada (biblioteca comunitaria), este jueves 16 de marzo a las 13 hrs. 


**¿Dónde se encuentra?** Dentro de la biblioteca del Instituto de Ecología, en el pasillo afuera del acervo.

**¿Cómo funciona?** Escojan el libro que les interese, se registra el préstamo usando el QR que encontrarán en las primeras páginas. Al terminarlo se devuelve ¡ustedes deciden el plazo de entrega!

**¡Te invitamos a donar!** Si tienes algún libro (relacionado con género y/o diversidades) que quisieras donar a nuestra biblioteca, escríbenos a cinig@ecologia.unam.mx

<img src='/media/Biblioteca_morada.jpg'width="90%">




