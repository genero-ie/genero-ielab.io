---
title: Diversidad sexo-genérica e inclusión en espacios educativos
date: 2021-06-18 12:00
category: Comunicados
---

<img src='/media/cartel_diversidad_sexo_generica.png' width="90%">

En el marco del Día Internacional del orgullo LGBTTTIQ+ el Instituto
de Investigaciones Biomédicas y las Comisiones Internas de Igualdad de
Género Invitan a la Conferencia:

**Diversidad sexo-genérica e inclusión en espacios educativos**

Impartida por la Dra. Siobhan Guerrero Mc Manus, investigadora del
Centro de Investigaciones Interdisciplinarias en Ciencias y
Humanidades (CEIICH-UNAM).

La cita es el lunes 28 de junio 2021, 12:00 hrs.




Descarga el [cartel en pdf](/media/cartel_diversidad_sexo_generica.pdf).


**Video de conferencia:**


<iframe width="560" height="315" src="https://www.youtube.com/embed/GhNzGZzVsxU?start=1" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
