---
title: Donaciones para Biblioteca Morada
date: 2023-04-26
category: Comunicados
---

A continuación, te compartimos cuáles son los criterios para poder hacer donaciones a este proyecto comunitario:


-Se reciben todo tipo de libros que estén escritos por mujeres.

-También aquéllos con autoría de personas que formen parte de la comunidad LGBTIQ+

-Los que aborden temas relacionados con diversidades y/o género, independientemente de la autoría.

Otras indicaciones importantes:

De preferencia, agregar una breve dedicatoria en las primeras hojas, firmada por su donante (o tal vez por el o la autora). Ejemplo: "Para la biblioteca morada de ..."

Los libros que desees donar puedes llevarlos directamente a la biblioteca del IE o contáctanos a cinig@ecología.unam.mx para gestionar tu donación

¡Muchas gracias por tu interés!
