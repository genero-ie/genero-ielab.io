---
title: Seminario inter-institucional de género, 25 de noviembre
date: 2020-11-20 13:00
category: Comunicados
---


Con motivo de la conmemoración del **Día Internacional de la Eliminación de la Violencia contra la Mujer** el 25 de noviembre (#25N), la Comisión
de Igualdad de Género del Instituto de Ecología (CInIG) ha decidido
organizar 3 actividades que se llevarán a cabo los miércoles a las 17
horas, iniciando la próxima semana (miércoles 25 de noviembre). En este
día tendrá lugar el segundo seminario inter-institucional de género,
organizado por las CInIGs de los Institutos de Biología, Ecología y
Fisiología Celular ¡esperamos contar con su asistencia!

<img src="/media/n25_ifc.jpg" width="95%" alt="Cambios en la UNAM relacionados con igualdad y violencia de género ¿Qué necesitamos saber? Ponente: Rosalba Cruz Martínez, abogada integrante de la Defensoría de los Derechos Universitarios, Igualdad y Atención de la Violencia de Género, UNAM."/>

Sintonízalo por [youtube](https://www.youtube.com/channel/UCexVPLCSwS2poahjxuewZNg). O contáctanos para participar por Zoom.
