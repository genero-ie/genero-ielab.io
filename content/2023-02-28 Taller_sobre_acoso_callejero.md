---
title: Adquiere herramientas para intervenir en una situación de acoso callejero
date: 2023-03-06
category: Comunicados
---

Seminarios interinstitucionales de igualdad de género les invitan, este **jueves 9 de marzo a las 16:00 hrs**, al taller presencial **"Adquiere herramientas para intervenir en una situación de acoso callejero"** 

Impartido por: organización no gubernamental Casa Gaviota. 

Dirigido a: hombres y mujeres.

Lugar: auditorio Alonso Escobar Izquierdo, Instituto de Investigaciones Biomédicas


Regístrate aquí: <https://forms.gle/i4sXJwuLr5pQNTQGA> 

<img src='/media/Taller_acoso_callejero.jpeg'width="90%">

