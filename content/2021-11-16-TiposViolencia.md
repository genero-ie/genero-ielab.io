---
title: Sobre la violencia de género
date: 2021-11-16
category: Comunicados
---
Existen distintos tipos de **violencia de género**. Conócelos en esta infografía. 

<img src='/media/Tipos_de_violencia_de_genero.jpg'>
