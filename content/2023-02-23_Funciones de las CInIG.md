---
title: Conoce las funciones de las CInIG de la UNAM
date: 2023-03-06
category: Comunicados
---

La **Comisión Interna para la Igualdad de Género** del **Instituto de Matemáticas** les invita este próximo **miércoles 8 de marzo a las 12 hrs** a la conferencia presencial **"Conoce las funciones de las Comisiones Internas para la Igualdad de Género de la UNAM"**, que se llevará a cabo en el auditorio Alfonso Nápoles Gándara del Instituto de Matemáticas.


Registrate aquí <https://forms.gle/yngU4YoMYY5wz8iu6> 

<img src='/media/ConferenciaMate.jpeg'width="90%">
