---
title: Corresponsabilidad
date: 2021-11-22
category: Comunicados
---

Para conocer más sobre la **corresponsabilidad** les invitamos a ver este video, que además de información, nos muestra algunas vivencias y ejemplos. 


<iframe width="560" height="315" src="https://www.youtube.com/embed/tzN5mnQ8Elg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

