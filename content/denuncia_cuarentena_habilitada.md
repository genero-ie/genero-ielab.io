---
title: Habilitada denuncia contra violencia de género en nuestra universidad
date: 2020-08-05 12:41
category: Comunicados
author: Rodrigo García
---

La Gaceta UNAM ha publicado un acuerdo, en el cual se menciona que desde el día 27 de julio queda habilitado de manera no presencial el
procedimiento de denuncia de violencia de género en la UNAM. 

[Leer acuerdo](https://www.gaceta.unam.mx/wp-content/uploads/2020/07/200727-acuerdo-por-el-que-se-habilitan-diversas-actividades-y-tramites-no-presenciales.pdf).
