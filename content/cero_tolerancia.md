---
title: Pronunciamiento de cero tolerancia
date: 2020-11-24 12:00
category: Comunicados
---


A la comunidad del Instituto de Ecología:


Por este medio, la Comisión Interna para la Igualdad de Género (CInIG)
y la Dirección del IE, hacemos explícito el pronunciamiento de cero
tolerancia hacia la violencia contra las mujeres en general, y
particularmente de nuestra Universidad.  La violencia contra las
mujeres y las niñas es una práctica extendida, arraigada y tolerada en
el mundo, que constituye una grave violación de los derechos humanos.


Ante esto corroboramos nuestro compromiso para impulsar la
implementación de la política institucional en materia de igualdad de
género de la Universidad, así como prevenir cualquier tipo de
discriminación y violencia por razones de género, a través de acciones
y la participación conjunta de las autoridades y la comunidad.