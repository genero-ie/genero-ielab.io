---
title: Los hombres universitarios ante la transición de género
date: 2022-09-13
category: Comunicados 
---

Seminarios Interinstitucionales de Igualdad de Género les invitan este 22 de septiembre 2022
al conversatorio-taller presencial  **"Los hombres universitarios ante la transición de género"** 



<img src='/media/Hombres_universitarios_ante_la_transicion_de_genro_seminario.jpeg'width="90%">


**Video del conversatorio:**

<iframe width="560" height="315" src="https://www.youtube.com/embed/DyYGaP0sEg4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
