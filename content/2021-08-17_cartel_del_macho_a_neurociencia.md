---
title: Del macho cazador a las neurociencias y las habilidades visuo-espaciales
date: 2021-08-17
category: Comunicados
---

Seminarios institucionales de igualdad de género invitan a la conferencia virtual en vivo:

**Del macho cazador a las neurociencias y las habilidades visuo-espaciales**

<img src="/media/cartel_2021-08-17.jpg" width="100%" />


**Video del seminario:**


<iframe width="560" height="315" src="https://www.youtube.com/embed/1QZD3_hDUjA?start=1" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

