---
title: Proyección: Ahora que estamos juntas
date: 2023-02-28
category: Comunicados
---

Se les hace una cordial invitación a la proyección del film "Ahora que estamos juntas", que se llevará a cabo el día **2 de marzo a las 11 hrs** en el **Jardín Botánico del IB UNAM**, como parte de las actividades del **#8M**


¡Les esperamos!


<img src='/media/proyección.jpeg' width="90%"> 
