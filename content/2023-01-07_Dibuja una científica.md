---
title: Concurso de dibujo: ¿Cómo es el trabajo de una científica?
date: 2023-01-07
category: Comunicados
---

¿Cómo es el trabajo de una científica? esta convocatoria es para que niñas de 7 a 14 años nos compartan su amor e interés por la ciencia a través de un dibujo.
Más informes en el cartel y en el QR.
La fecha límite de recepción de dibujos es el 11 de febrero



<img src='/media/trabajo_de_una_científica.jpeg'> 
