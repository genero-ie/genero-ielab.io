---
title: Pronunciamiento cero tolerancia
date: 2021-11-24
category: Comunicados
---



A la comunidad del Instituto de Ecología,

Por este medio, la Comisión Interna para la Igualdad de Género (CInIG) y la Dirección del IE, hacemos explícito el pronunciamiento de cero tolerancia hacia la violencia contra las mujeres en general, y particularmente contra las mujeres de nuestra Universidad. 

Adicionalmente, en este año que se conmemoran 500 años de resistencia indígena, es pertinente señalar que las mujeres indígenas son particularmente vulnerables y han sido víctimas históricas de violencia en el contexto de la defensa de la tierra comunitaria y los derechos ambientales. En concreto, es notable mencionar que casi la mitad de activistas que se han asesinado han sido mujeres indígenas en lucha por el medio ambiente. Esta violencia en contra de muchas defensoras pasa en gran medida desapercibida y hoy aprovechamos la oportunidad para hacerlo notar.

La violencia de género es contraria a la Legislación Universitaria. Habrá cero tolerancia ante actos de esta naturaleza cometidos por cualquier integrante de la comunidad, y éstos serán sancionados conforme a lo establecido en la normatividad aplicable.
