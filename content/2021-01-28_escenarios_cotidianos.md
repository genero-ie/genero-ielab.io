---
title: Ciclo de Conferencias
date: 2021-01-28 12:00
category: Comunicados
---

Los invitamos al ciclo de conferencias **Escenarios en la vida cotidiana: nuevos retos para una convivencia sin violencia.**

![Escenarios en la vida cotidiana: nuevos retos para una convivencia sin violencia.](/media/cartel_conferencia_escenarios_cotidianos_2.jpg)

![Escenarios en la vida cotidiana: nuevos retos para una convivencia sin violencia.](/media/cartel_conferencia_escenarios_cotidianos_1.jpg)

