---
Title:Ganadoras del concurso de dibujo ¿Cómo es el trabajo de una científica? 
date: 2023-02-22
category: Comunicados
---
Es de nuestro agrado presentarles a continuación los dibujos seleccionados como ganadores del concurso **¿cómo es el trabajo de una científica?**. 

Muchas felicidades a las participantes, **Ámbar Piñón Lira** y **Fátima Yamilet Ruiz Escamilla**, por su talento y las ganas de compartir a través de su arte. 

<img src='/media/dibujo1.jpeg'width="90%">
<img src='/media/dibujo2.jpeg'width="90%">



