---
title: Día internacional del orgullo LGBTQI+
date: 2022-05-19
category: Testimonios
---

Convocamos a las diversidades sexogenéricas, así como a las
diversidades relacionales, mentales, físicas y cualquier otra forma de
diversidad a romper con la falsa imagen de homogeneidad y de lo
"normal", invitando a otrxs a ponerse en tus zapatos, a mirar y
entender el mundo de otra manera: ¿cómo eres? ¿qué te hace diferente?
¿qué te gustaría decir y compartir para sentirte libre en tu
comunidad?

Este mes de junio, cuéntanos tu experiencia desde tu
diversidad. Publicaremos los videos y testimonios en la página de
nuestra CInIG y en las redes sociales del Instituto de Ecología.

## Envíanos un video

Con una duración máxima de 5 minutos, aquí:

<actividades_cinig@ecologia.unam.mx>


## Compártenos un testimonio escrito

Puede ser anónimo. En los buzones del Instituto o en esta liga:

<https://forms.gle/fSRJC15iK2fLfWoC7>


**Hay muchos silencios por romper ¡Esperamos tu contribución! ¡participa!**


<img src="/media/convocatoria_orgullo_2022.png" width="80%">
