---
title: Presentación de las POC en el Instituto de Ecología
fecha: 2022-09-06
category: Comunicados
---

¿No sabes a quién acudir ante un caso de violencia por razones de género? 
Contacta a las **Personas Orientadoras Comunitarias (POC)** de nuestra comunidad.

<img src='/media/Poc_2024.jpg'width="90%">


______________

Última actualización: enero 2024
