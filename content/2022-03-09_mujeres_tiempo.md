---
title: Mujeres en el tiempo 2022 #8M 
date: 2022-03-09
category: Comunicados
---

Con motivo de las actividades del #8M, hacemos una cordial invitación a formar parte del álbum **Mujeres en el tiempo**. ¡Formar parte es muy fácil! Envía tu foto, tu nombre completo y una breve descripción. 
La recepción de fotos será durante todo el mes de marzo a través del correo contacto@iecologia.unam.mx 


<img src='/media/photo_2022-03-09_15-06-31.jpg' width="90%">
