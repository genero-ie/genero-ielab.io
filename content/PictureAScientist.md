---
Title: Cine-debate y proyección de la película "Picture a scientist"
Date: 2020-12-02 11:00
Category: Comunicados
---

El miércoles 9 de diciembre a las 17hrs se llevó a cabo esta tercera y última
actividad conmemorando el #25N. La comentarista y moderadora del
cine-debate fue María Emilia Beyer Ruiz, directora de Universum,
Museo de las Ciencias de la UNAM.

<img src="/media/cinedebate_picture_a_scientist.png" width="95%" />

**Video del cine-debate**

<iframe width="560" height="315" src="https://www.youtube.com/embed/onk3KAhxAxI" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
