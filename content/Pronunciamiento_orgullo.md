---
title: Día del orgullo, pronunciamiento del IE
date: 2021-06-28
category: Comunicados
---
El día del orgullo (28 de junio) es un día para conmemorar la lucha de la defensa de los derechos LGBTTIQ+. La dirección del Instituto de Ecología junto con la CInIG, nos pronunciamos por una universidad respetuosa de las diversidades sexuales y que garantice los derechos humanos, y ratificamos nuestro compromiso para fomentar la igualdad de género, la inclusión y la no discriminación.

















