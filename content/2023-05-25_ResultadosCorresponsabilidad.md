---
title: Resultados encuesta de corresponsabilidad
date: 2023-05-25
category: Comunicados
---

Agradecemos a la comunidad por haber participado en la encuesta en línea **“Corresponsabilidad en las labores domésticas y de cuidado, y su efecto en el trabajo y/o trayectoria académica”**, aplicada en febrero de 2022.

A continuación les presentamos los resultados.  Para acceder al pdf con los resultados dar click [aquí](https://institutodeecologia584-my.sharepoint.com/:b:/g/personal/no_reply_equidad_ecologia_unam_mx/ER9p9hBJ44dNsSi-zZ40elQBZZHEquohkm1KHOK-qabc_g?e=PAALv3) 
