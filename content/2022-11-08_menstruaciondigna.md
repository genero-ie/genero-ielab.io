---
title: Menstruación digna: asunto público
date: 2022-11-08
category: Comunicados
---
Seminarios Interinstitucionales de Igualdad de Género les invitan este proximo jueves 10 de noviembre a las 16:00 hrs a la conferencia virtual "Menstruación digna: asunto publico".


<img src='/media/seminario10.jpg'width="90%">

**Video del seminario**
<iframe width="560" height="315" src="https://www.youtube.com/embed/x_EbhXxXTYk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> 
