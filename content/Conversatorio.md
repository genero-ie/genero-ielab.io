---
title: ¿Qué hacer? Reflexión colectiva sobre los retos de las mujeres del IE durante la pandemia
date: 2020-11-25 12:00
category: Comunicados
---

Aquí podrán ver el conversatorio
que se llevó a cabo el miércoles 2 de dicimbre a las 17
hrs.
Contamos con la participación de las moderadoras Adriana Ruiz y
Camila Ramírez, de la carrera de Desarrollo y Gestión Interculturales;
las relatoras/sistematizadoras Mariana Benítez y Blanca Hernández
Hernández, integrantes de nuestra comunidad; y panelistas
representando a estudiantes, personal de confianza, personal de base,
investigadoras y técnicas académicas del instituto.


**Video del conversatorio**

<iframe width="560" height="315" src="https://www.youtube.com/embed/45YEvrLqqr8" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>







