---
title: Violentómetro
date: 2023-03-01
category: Comunicados
---

Compartimos este violentómetro que las POC del IE han preparado para su difusión en nuestra comunidad. 

<img src='/media/violentometro.jpg'>
