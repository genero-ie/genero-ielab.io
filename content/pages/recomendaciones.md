---
title: ¿Necesitas ayuda?
---

 
# Recomendaciones generales si eres víctima de violencia

 - Hablar de tu situación con alguna persona de confianza y establecer entre ustedes un código de emergencia (palabra clave, emoji, frase, sticker, etc) para que llame a la policía.

 - Comunicarse diariamente con familia o amigos cercanos. Infórmales si durante este periodo se incrementa la violencia doméstica-física, psicológica o sexual, y sientes que tu vida está en riesgo. No lo escondas ni te acostumbres por que no va a parar: solo irá en aumento.

 - Tener a la mano los números telefónicos y redes sociales de instituciones públicas y privadas que ofrecen ayuda y orientación. Tener preparada una maleta que incluya duplicado de las llaves, actas de nacimiento (propia y de hijas e hijos si es el caso), acta de matrimonio (si aplica), carnet del seguro social e identificación oficial, procedimiento judicial en trámite (si aplica) y medicinas.

 - La violencia doméstica es progresiva, es decir, escala y no retrocede. Por ello, no esperes: pide ayuda, sin que importen el día ni la hora.


# Recomendaciones para las y los aliados 

 - Es seguro estar al pendiente de la víctima vía remota: 
      - WhatsApp,
      - correo electrónico o celular.
 - También es seguro enviar información que distraiga a la víctima de su situación: 
      - videos divertidos,
      - memes,
      - por último se sugiere y es seguro escribir mensajes discretos que le recuerden a la víctima que su vida es muy importante.
  
 - Estar atentos y atentas en caso de recibir la palabra clave para informar a las autoridades.


# Teléfonos y redes de emergencia

## Emergencias inmediatas (no todos estos contactos se especializan en violencia de género)
 
 - Cruz Roja: 065

 - Emergencia Policial: 911

 - CTA Centro de Terapia de Apoyo a Víctimas de Delitos Sexuales: 55 52 00 96 32 y 55 52 00 96 33 de 9:00 a 19:00 hrs de lunes a viernes
<https://www.fgjcdmx.gob.mx/nuestros-servicios/en-linea/mp-virtual/cta-centro-de-terapia-de-apoyo-victimas-de-delitos-sexuales>


 - Para el trámite de Medidas de Protección está disponible el celular de una de las abogadas de Línea Mujeres, las 24 hrs; marcar al 55 55 12 27 72 ext. 123

 - Locatel: *0311 o 55 56 58 11 11 y solicita la Línea Mujeres de los Servicios Especializados, disponible las 24 horas del día, de lunes a domingo, incluidos días festivos. Por vía chat, disponible de 10 a 18 horas, los 365 días del año en <https://cms311.cdmx.gob.mx/linea-mujeres/>

  - Unidad de Investigación de Delitos Sexuales cometidos en agravio de estudiantes de educación media superior y superior. Horario: 9am a 7pm de lunes a viernes. Tel. 55 52 00 96 39. Dirección: Enrique Pestalozzi  #1115 Col. Del Valle 

                                                       

## Si eres universitarix de la UNAM

 - Ruta de atención de quejas por violencia de género:                
      - Consúltala en la liga de la página de la Defensoría <https://www.defensoria.unam.mx/web/ruta-queja-violencia-genero>
      - Contacta a la Defensoría de los Derechos Universitarios: 55 41 61 60 48 (con 40 líneas), marca 2 para atención de violencia de género. Correo: defensoria@unam.mx
      - Consúltala en la liga de la página de la CIGU <https://coordinaciongenero.unam.mx/ruta-atencion-violencia-genero/>


 - Pak’te (si eres o fuiste alumnx de la facultad de Ciencias):  <https://sites.google.com/ciencias.unam.mx/covid19-fc/home/violencia-de-g%C3%A9nero-y-dom%C3%A9stica>

 - Para quienes viven en el norte de la CDMX, FES Iztacala tiene un servicio al público de atención a la violencia sexual en la Clínica de la Facultad (PIIAV). Teléfono para citas: 55 56 23 11 02. 
<https://www.iztacala.unam.mx/piav/introduccion.html>



## Atención dentro y fuera de la UNAM

**Asesoría jurídica y psicológica (se ofrecen las dos) y contactos de refugios**

 - Lunas <https://www.semujeres.cdmx.gob.mx/servicios/servicio/lunas>

 - Línea HÁBLALO, las 24 hrs: 800 422 52 56

 - Centro de Apoyo a la Mujer Margarita Magón, A.C.
55 55 19 58 45 Dirección: Carlos Pereyra #113 Colonia Viaducto Piedad 

 - Centros de Justicia para las Mujeres CDMX:  Azcapotzalco: 55 53 46 83 94 ; Iztapalapa: 55 53 45 57 37 ; Tlalpan: 55 52 900 92 80

 - Red Intercultural de Refugios: 800 836 88 80

 - Red Nacional de Refugios AC: 55 56 74 96 95 / 55 52 43 64 32 (CDMX) y 800 822 4460 (Línea Nacional); Correo: renarac@rednacionalderefugios.org.mx ; Twitter: @RNRoficial ; Facebook: @RedNacionalDeRefugiosAC ; Instagram: @redrefugiosMX


**Asesoría jurídica**

 - ADEVI Centro de Apoyo Sociojurídico a Víctimas del Delito Violento: 55 52 00 91 96 / 55 52 00 91 97 / 55 52 00 92 00 en un horario de 9:00 a 19:00 horas de lunes a viernes <https://www.atencionalaviolencia.semujeres.cdmx.gob.mx/>

 - Centros de Justicia para las mujeres en el Estado de México: Cuautitlán Izcalli: 55 58 68 33 28 ; Ecatepec: 55 57 87 78 02 ; Toluca: 72 22 83 20 07 

 - Bufete jurídico gratuito, de la Facultad de Derecho (no tienen capacitación en género):  
      - Ciudad Universitaria:
      Planta baja del Edificio anexo B de Facultad de Derecho,
      Horario de atención de 10:00 a 13:00 horas
      bufetejuridico@derecho.unam.mx
      
      - Centro Histórico:
      Planta baja del Edificio de la Escuela Nacional de Jurisprudencia
      San Idelfonso No. 28, esquina con Argentina, Centro Histórico, CDMX
      Horario de atención de 10:00 a 13:00 horas
      Tel.  57 02 57 42       
      
      - Xochimilco:
      Calle de maíz sin número
      Col. Santa Cruz Xochitepec,  Del. Xochimilco, CDMX
      (Clínica periférica de odontología UNAM)
      Horario de atención de 10:00 a 13:00 horas
      Tel. 56 76 84 96

 - Unidad de Apoyo Jurídico (UAJ) (no tienen capacitación en género), las 24 hrs: 56 16 17 84 y 56 22 24 47; Celular: 55 34 00 95 59; correo: uapoyojuridico.daj@unam.mx

**Asesoría familiar y laboral**

 - Abogadas con glitter (cobran una cuota accesible) y ofrecen asesoría con perspectiva de género:
     Facebook: <https://www.facebook.com/AbogadasConGlitter> ;
     Instagram: <https://www.instagram.com/abogadas.con.glitter> ;
     Correo: abogadasglitter@gmail.com

 - Defensa Jurídica y Educación para Mujeres, Vereda Themis, S. C. 55 53 41 65 70 / 55 53 96 55 86 Av. de los Maestros, 91-A, Col. Agricultura,Miguel Hidalgo, CDMX (a tres cuadras del metro Normal) <https://veredathemis.org.mx>

 - Colectiva de abogadas feministas (ofrecen asesoría jurídica feminista). WhatsApp: 55 28 48 32 28

 - PROFEDET (Procuraduría Federal de la Defensa del Trabajo) Dr. José María Vértiz 211 Doctores 06720 CDMX. Teléfono atención ciudadana: 800 717 2942 y 800 911 7877 <https://www.gob.mx/profedet>

 - Agencia especializada para la Atención de Personas Adultas Mayores Víctimas de Violencia Familiar: 53 45 51 11 ext. 5111 de 09:00 a 17:00 hrs de lunes a viernes
     <https://www.fgjcdmx.gob.mx/micrositios/agencia-para-la-atencion-de-personas-adultas-mayores/servicios>

 - CAVI Centro de Atención a la Violencia Intrafamiliar: 55 53 45 52 48 / 55 53 45 52 28 / 55 53 45 52 29 ; de 9:00 a 19:00 hrs todos los días
 <https://www.fgjcdmx.gob.mx/cavi>


**Asesoría para hombres**

 - Línea para hombres que atraviesan crisis de ansiedad, estrés y depresión y/o requieren asesoría jurídica para conocimiento de las consecuencias de ejercer o recibir violencia doméstica: 800 900 43 21

 - Línea y chat para hombres víctimas de violencia familiar: 55 55 33 55 33

 - Cursos GENDES (sobre masculinidades)   <https://gendes.org.mx/> Cuenta con grupos de atención presencial (aportación solidaria): lunes, martes, miércoles 19:00 a 21:30 hrs ; sábado de 10:00 a 12:30 hrs. Teléfono: 55 55 84 06 01. Correo: info@gendes.org.mx . Además, cuenta con línea de emergencia para hombres (si estás triste, enojado o en tensión): 55 47 57 92 88

 - PROITH UNAM (Programa Integral de Trabajo con Hombres) <https://coordinaciongenero.unam.mx/programa-integral-trabajo-con-hombres/> Ofrece grupos de trabajo (correo: trabajoconhombres@unam.mx), y círculos de reflexión para hombres (correo: proith@unam.mx) 

**Atención psiquiátrica y/o psicológica para universitarios (UNAM)**

 - Consulta opciones de centros de atención psicológica aquí: <https://coordinaciongenero.unam.mx/2021/08/atencion-psicologica-unam/>

 - Clínica de atención a la violencia de género. Facultad de Medicina. Correo: clinicadegenero@facmed.unam.mx

 - Facultad de Psicología, programa de sexualidad humana (PROSEXHUM). Correo: prosexhum.psicologia@unam.mx; Teléfono: 55 56 22 22 89. Responsable:  Ena Eréndira Niño Calixto, correo: nino.ena.60@gmail.com Este programa ofrece: consejo breve sobre salud sexual, intervención en crisis por violencias de género y psicoterapia en violencia de género. Existen 2 formas de pedir cita: 
      - Hablar al teléfono aquí proporcionado, y/o escribir a la responsable (Mtra. Niño). 
      - Contestar este cuestionario <https://www.misalud.unam.mx/> y escribir a la responsable del servicio atención psicológica (Zoraida Meléndez Zermeño), al correo zoraidamelendez72@gmail.com . En el cuerpo de correo debe indicarse la solicitud de cita, así como el folio o ID que se genera al terminar el cuestionario. 

 - Residencia en Terapia Familiar de FES Iztacala. Correo : iztacalaterapiafamiliar@gmail.com


**Atención psiquiátrica y/o psicológica fuera de la UNAM**

 - SORECE Asociación de Psicólogas Feministas. Salud mental desde una perspectiva feminista interseccional. Teléfono 55 51 61 86 00 / 55 32 37 32 18 WhatsApp 55 32 37 32 18
<https://sorece-ac.org/>

 - Instituto Mexicano de la Juventud (CONTACTO JOVEN) Para jóvenes, ofrece atención en casos de estrés, ansiedad y manejo de emociones. WhatsApp 55 72 11 20 09 (escribe la palabra "CONTACTO"). En caso de violencia o uso de sustancias, el Instituto derivará a Centros de Integración Juvenil y Línea de la Vida. 

 - Apoyo psicológico del Consejo Ciudadano Para la Seguridad y Justicia de México. Teléfono 55 55 33 55 33 <https://consejociudadanomx.org/servicios/apoyo-psicologico-626ffa1400068>

 - Marca al *0311 <https://cms311.cdmx.gob.mx/asesorias-especializadas/>


**Diversidad segura**

 - Línea Nacional Diversidad Segura. Teléfono: 800 000 5428


**Fiscalías**

 - Fiscalía Central de Investigación para Atención de Delitos Sexuales: 53 45 51 36 y 53 46 81 16	

 - Fiscalía Central de Investigación para la Atención del Delito de Trata de Personas: 55 53 46 81 10 y 55 53 46 84 80

 - Fiscalía Central de Investigación para la Atención de Niños, Niñas y Adolescentes: 52 42 61 21 ext. 6121 las 24 hrs (agencia 59) 

 - Fiscalía Especial para los Delitos de Violencia contra las Mujeres y Trata de Personas (FEVIMTRA) 55 53 46 25 16

 - Fiscalía Especializada para la Investigación para el Delito de Feminicidio: 55 53 45 59 65 y 55 53 45 59 77

 - Fiscalía General de Justicia de la Ciudad de México: 55 52 00 90 00; denuncia de forma anónima: 089


**Violencia Digital**

 - Denuncia de extorsión o fraude digital (Guardia Nacional): 089 / 088

 - Denuncias de Ley Olimpia (acoso en internet) en CDMX:
<https://denunciadigital.cdmx.gob.mx>

 - Luchadoras (un espacio libre de violencias, un espacio donde creamos redes):
<https://luchadoras.mx/internetfeminista/herramientas/>

 - Policía cibernética de la CDMX: 55 52 42 51 00 ext. 5086 Correo : policia.cibernetica@ssc.cdmx.gob.mx

 - Policía cibernética del Estado de México: 72 22 75 83 33 Correo: cibernetica.edomex@ssedomex.gob.mx

 - Unidad de Inteligencia Cibernética CDMX: 55 52 42 64 89 Correo: ciberneticapdi@fgjcdmx.gob.mx













————————————————————————————

Escríbenos a <cinig@ecologia.unam.mx> si requieres más información sobre este directorio. Contacta a las POC del IE ante casos de violencia por razones de género <https://genero.ecologia.unam.mx/presentacion-de-las-poc-en-el-instituto-de-ecologia.html> 



Última actualización: septiembre del 2023



