---
title: Estadísticas
---


Tabla de quejas previas al Protocolo / Ruta de Atención para quejas por violencia de género

<a href="/media/DatosViolencia1_R.jpg"><img src="/media/DatosViolencia1_R.jpg" width="100%" /></a>



<strong>Tabla de quejas registradas de 2018 a 2024, actualizada al 27 de noviembre de 2024</strong>

<a href="/media/DatosV_2024.png">
<img src="/media/DatosV_2024.png" width="100%" />
</a>



[Descargar hoja de cálculo](/media/Quejas_2024_1.ods).



Los datos que se publican se encuentran ajustados a lo dispuesto por el "Acuerdo del Comité de Transparencia de la UNAM",en atención a la circular UT/06/2021 hecha por la Unidad de Transparencia, y tiene por objeto recolectar y transparentar, mediante la concentración y reproducción de información estadística (con datos debidamente desagregados), que permiten identificar los casos presentados ante la UNAM por conductas susceptibles de violencia de género, los procedimientos iniciados y las sanciones recaídas a éstas, después de su estudio y análisis. En ese tenor, resulta crucial poner a disposición de las mujeres todos aquellos elementos informativos que les permitan ejercer plenamente sus derechos.

Es necesario precisar que, tal y como lo hace la Comisión Interamericana de Derechos Humanos (CIDH), el derecho de acceso a la información se encuentra sujeto a restricciones, en tanto que no es un derecho absoluto. Por lo que, conforme al marco normativo vigente en las materias de transparencia, acceso a la información y protección de datos personales, entre dichas restricciones es posible encontrar, la protección de los datos personales y aquella información que pudiera vulnerar los denominados "derechos de la personalidad" como lo son: la vida privada y/o intimidad, el honor y la propia imagen.


La protección de los datos personales y sensibles es una obligación reconocida a nivel nacional e internacional, de conformidad con el artículo 12 de la Declaración Universal de los Derechos Humanos; el artículo 11 de la Convención Americana sobre los Derechos Humanos; el artículo 17 del Pacto Internacional de los Derechos Civiles y Políticos; el artículo 116, primer y segundo párrafos de la Ley General de Transparencia y Acceso a la Información Pública; 113, fracción I, y último párrafo de la Ley Federal de Transparencia y Acceso a la Información Pública; numerales 6, 7, 17 y 18 de la Ley General de Protección de Datos Personales en Posesión de Sujetos Obligados; el artículo 40 del Reglamento de Transparencia y Acceso a la Información Pública de la UNAM; numeral 2, fracción II, y numeral 9 de los Lineamientos para la Protección de Datos Personales en Posesión de la Universidad Nacional Autónoma de México.


Asimismo, resulta aplicable la Jurisprudencia, emitida por la SCJN intitulada "Derecho Fundamental al Honor. Su dimensión subjetiva y objetiva" de la Décima época, registro: 2005523, instancia: Primera Sala, tipo de tesis: Jurisprudencia, fuente: Gaceta del Semanario Judicial de la Federación, libro 3, febrero de 2014, tomo I, materia(s)
Constitucional, Tesis: 1a/J. 118/2013
