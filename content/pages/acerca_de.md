---
title: Acerca de la Comisión 
---

## Objetivo General de la CInIG IE

Impulsar la implementación de la política institucional en materia de
igualdad de género de la Universidad y prevenir cualquier tipo de
discriminación y violencia por razones de género, a través de acciones
sistemáticas y profundas diseñadas con la participación de las
autoridades y la comunidad.

Más sobre las CInIG de la UNAM en la [página de la Coordinación para la Igualdad de Género](https://coordinaciongenero.unam.mx/cinigs/).


## Transparencia

Desde el marco de transparencia que la UNAM establece, nuestras actas
de juntas, planes e informes, quedan a su disposición en los siguientes enlaces:

 - [Actas de nuestras sesiones de trabajo](https://institutodeecologia584-my.sharepoint.com/:f:/g/personal/no_reply_equidad_ecologia_unam_mx/EmdQhLgORBtPsoTTYuMlaL8BCGWVKZVZRwqc27sdIAPoCA?e=PCF3R9)

 - [Planes de trabajo e informes](https://institutodeecologia584-my.sharepoint.com/:f:/g/personal/no_reply_equidad_ecologia_unam_mx/EvhOeefDMJVDm3vRYEnsPm0BGSLywJhcjzMdLc0AeeFKQg?e=2kAl7d)


## Manual CInIG IE

[El manual del la CInIG IE](/media/manual_cinig_ie.pdf) fue aprobado en Consejo Interno en sesión ordinaria de [noviembre del 2022](/media/14_2022.pdf)

## Conformación

Con base en los [Lineamientos generales para guiar la conformación y el funcionamiento de las Comisiones Internas para la Igualdad de Género en entidades académicas y dependencias universitarias de la UNAM](https://drive.google.com/file/d/1tPidjtS4yaKAS1ODHMn4rN2TXRD90xvx/view),
el Consejo Interno del Instituto de Ecología, en sesión ordinaria de 
[junio del 2023](/media/7_2023.pdf) aprobó adecuar a la Comisión Interna para la Igualdad de Género, de acuerdo a la [nueva acta de conformación](/media/ConformacionCInIGIE_2023_firmada.pdf) quedando integrada por los siguientes


### Miembros

| | |
|---|---|
|Titular de la dirección| Ana Elena Escalante Hernández |
| Integrante del Consejo Interno| [Alejandro González Voyer](http://132.248.49.121/perfiles/perfil.php?ID=1453323443441)|
| Integrante sector académico (representante) | Edgar G Ávila Luna | 
| Integrante sector estudiantil| Gloria Alejandra Sarmina Leone | 
| Integrante sector estudiantil| Karla Adriana Peña Sanabria | 
| Integrante sector estudiantil| María Aimé Rubini Pisano | 
| Integrante sector administrativo| Diana G. Martínez |
| Integrante sector administrativo| Nayeli Ramírez Salinas |


____________________________________
Última actualización: diciembre 2023









