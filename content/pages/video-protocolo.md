---
Title: Protocolo

---


Si quieres conocer más sobre el tema te invitamos a ingresar a los siguientes links: 


[**Protocolo para la atención integral de casos de violencia por razones de género en la UNAM**](https://www.defensoria.unam.mx/web/documentos/protocolo-atencion-integral-de-violencia-por-razones-de-genero.pdf).


[**Ruta de Atención en la página de la Defensoría de los Derechos Universitarios**](https://www.defensoria.unam.mx/web/ruta-queja-violencia-genero).



[**Ruta de Atención en la página de la Coordinación para la Igualdad de Género UNAM**](https://coordinaciongenero.unam.mx/ruta-atencion-violencia-genero/).


<img src="/media/ruta_atencion.png" width="100%" />







