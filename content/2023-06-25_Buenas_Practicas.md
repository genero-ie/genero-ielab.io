---
title: Cartilla de buenas prácticas LGBTIQ+
date: 2023-06-25
category: Comunicados
---

¡Ya está disponible la Cartilla Universitaria de Buenas Prácticas enfocadas a Poblaciones LGBTIQ+! La puedes encontrar en la página de la Abogacía General: <http://www.abogadogeneral.unam.mx/LGBTIQ>

