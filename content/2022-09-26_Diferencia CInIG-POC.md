---
title: Diferencias entre CInIG y POC
date: 2022-09-27
category: Comunicados
---

Les compartimos las diferencias entre las 
**CInIG** (Comisiones Internas para la Igualdad de Género) y las **POC** (Personas Orientadoras Comunitarias).

<img src='/media/Diferencia_POC-CInIG.png'width="90%">





