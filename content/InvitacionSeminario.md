---
title: Recordatorio charla este jueves 17 
date: 2020-09-14
category: Comunicados
---

Estimadas todas y todos,


Les recordamos que este jueves 17 de septiembre a las 17 hrs. se llevará a cabo el primer seminario inter-institucional sobre género, organizado por las Comisiones de Equidad de Género de los Institutos de Ecología, Biología y Fisiología Celular. Será impartido por la Dra. Amneris Chaparro, del del Centro de Investigaciones y Estudios de Género (CIEG), con el tema "Género, violencia y pandemia”. 


Pueden seguir la charla aquí:


<iframe width="745" height="419" src="https://www.youtube.com/embed/N4_bi-d5B6I" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


Un saludo, les esperamos el jueves.


Atentamente,


Comisión de Equidad de Género.


