---
title: Hablemos de nuestros derechos desde la perspectiva de género: acoso laboral
date: 2022-06-22
category: Comunicados
---

Seminarios institucionales de Igualdad de género les invitan el 2 de junio 2022 a la conferencia virtual 
"Hablemos de nuestros derechos desde la perspectiva de género: acoso laboral"

<img src='/media/acoso_laboral.jpeg'width="90%">


**Video del seminario:**


<iframe width="560" height="315" src="https://www.youtube.com/embed/e7HfxxAjE_g?start=1" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
