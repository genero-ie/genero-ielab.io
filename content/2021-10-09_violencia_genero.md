---
title: Definiciones relacionadas con género y violencia
date: 2021-11-09
category: Comunicados
---
Les invitamos a checar esta infografía con información sobre **sexo**, **género** y **violencia de género**. 

<img src='/media/Generoyviolencia.jpg'>
