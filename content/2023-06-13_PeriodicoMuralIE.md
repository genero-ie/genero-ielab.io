---
title: Periódico Mural 
date: 2023-06-13
category: Comunicados
---

¿Sabías que en el IE tenemos un periódico mural? 

Te invitamos a conocerlo, lo puedes encontrar al fondo del jardín. Ahora estamos compartiendo reseñas de libros que forman parte de la Biblioteca Morada.


<img src='/media/PeriodicoMuralIE_Resenas.jpeg'> 
